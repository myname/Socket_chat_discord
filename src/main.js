import { createApp } from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'

// store.dispatch('products/load')

const app = createApp(App)
app.use(store)
app.use(router)
// store.dispatch('loadIo')
// store.dispatch('products/load').then(() => { // если монтировать приложение только после загрузки
//     app.mount('#app')
// })
app.mount('#app')

import 'bootstrap/dist/css/bootstrap.css'
import 'animate.css'