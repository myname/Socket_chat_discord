import { makeRequest } from '@/api/server'

export default {
    namespaced: true,
    state: {
        items: []
    },
    getters: {
        all: state => state.items,
        getMap(state) {
            const map = {}

            state.items.forEach((pr, i) => map[pr.id.toString()] = i)
            return map
        },
        get: (state, getters) => id => state.items[getters.getMap[id]]
        // get: state => id => state.items.find(el => el.id == id)
    },
    mutations: {
        setItems(state, items) {
            state.items = items
        }
    },
    actions: {
        async load(store) {
            const items = await makeRequest('http://faceprog.ru/reactcourseapi/products/all.php')
            store.commit('setItems', items)
        }
    }
}