import { createStore } from 'vuex'
import { io } from 'socket.io-client'

// import products from './products'
// import cart from './cart'

// export default createStore({
//     modules: {
//         products,
//         cart
//     },
//     strict: process.env.NODE_ENV !== 'production'
// })

export default createStore({
    state: {
        socket: null,
        messages: [],
        onlineCount: null,
        // ioId: null,
        host: process.env.VUE_APP_HOST,
        clientId: process.env.VUE_APP_CLIENT_ID,
        connectedLocal: false,
        socketId: null,
        oauthData: null,
        user: null,
        errors: [] // ошибки для вывода на экран
    },
    getters: {
        socket: state => state.socket,
        messages: state => state.messages,
        onlineCount: state => state.onlineCount,
        // ioId: state => state.ioId,
        host: state => state.host,
        clientId: state => state.clientId,
        connectedLocal: state => state.connectedLocal,
        socketId: state => state.socketId,
        oauthData: state => state.oauthData,
        user: state => state.user,
        errors: state => state.errors
    },
    mutations: {
        setSocket(state, socket) {
            state.socket = socket
        },
        pushMessages(state, message) {
            state.messages.push(message)
        },
        emitMessages(state, message) {
            if (!state.socket?.connected || state.connectedLocal === false) throw new Error('Клиент не подключен.')
            // тут отправлять нужно только сообщение, остальное прикрепится на стороне сервера
            state.socket.emit('chat message to server', message)
        },
        setOnlineCount(state, onlineCount) {
            state.onlineCount = onlineCount
        },
        // setIoId(state, id) {
        //     localStorage.setItem('ioId', id)
        //     state.ioId = id
        // },
        setConnectedLocal(state, status) {
            state.connectedLocal = status
        },
        setSocketId(state, id) {
            state.socketId = id
        },
        setOauthData(state, oauthData) {
            state.oauthData = oauthData
        },
        setUser(state, user) {
            state.user = user
        },
        addErrors(state, err) {
            state.errors.push(err)
        },
        removeErrors(state, i) {
            state.errors.splice(i, 1)
        },
        disconnect(state) {
            state.connectedLocal = null
            state.onlineCount = null
            state.messages.push({msg: 'disconnect', user: state.user, type: 'system'})
            // store.commit('pushMessages', {msg: 'disconnect', user: store.state.user, type: 'system'}) // store нет тут
            setTimeout(() => { // ждем время, что бы не было ддоса подключениями
                state.connectedLocal = false
            }, 2000)
            state.socket.disconnect()
        }
    },
    actions: {
        async loadIo(store) {
            store.commit('setConnectedLocal', true)
            const socket = io()
            store.commit('setSocket', socket)
            // store.commit('pushMessages', {msg: 'join (frontend)', user: store.state.user, type: 'system'})

            // let count = 0 // тест
            // setInterval(() => {
            //     socket.volatile.emit("ping", ++count) // если есть volatile, то это аналог UDP
            // }, 1000)

            // socket.onAny((eventName, ...args) => {}) - слушатель на все события сразу
            // socket.prependAny - аналог onAny, только добавляется в начало массива слушателей
            // socket.off - удаляет слушатель; socket.removeAllListeners - удаляет все слушатели
            socket.on("connect", () => { // всякий раз когда клиент будет подключаться к серверу (socket.once - только один раз)
                console.log(socket.id) // id юзера что подключился
                store.commit('setSocketId', socket.id)
            })

            socket.on('disconnect', reason => {
                console.log(reason, 'reason')
                // store.commit('setIoId', null)
            })

            socket.on('chat message to client', function(msgData) {
                console.log('frontend', msgData.msg, +new Date())
                store.commit('pushMessages', msgData)
            })

            socket.on('connectUser', function(onlineCount) {
                console.log(onlineCount)
                store.commit('setOnlineCount', onlineCount)
            })

            // кста эти данные нам вроде и не нужны будут, но пусть
            socket.on('oauthData', oauthData => {
                store.commit('setOauthData', oauthData)
                console.log(oauthData)
                // localStorage.setItem('access_token', oauthData.access_token)
                // localStorage.setItem('expires_in', oauthData.expires_in)
                // localStorage.setItem('refresh_token', oauthData.refresh_token)
            })

            // кста эти данные нам вроде и не нужны будут, но пусть
            socket.on('user', user => {
                store.commit('setUser', user)
                console.log(user)
                // localStorage.setItem('id', user.id)
                // localStorage.setItem('username', user.username)
                // localStorage.setItem('discriminator', user.discriminator)
                // localStorage.setItem('avatar', user.avatar)
                // localStorage.setItem('locale', user.locale)
            })

            socket.on('error_to_client', errMessage => {
                store.commit('addErrors', errMessage)
                console.log(errMessage)
            })
        },
        async discconectIo(store) {
            store.commit('disconnect')
            console.log('disconnect')
        }
    },
    strict: process.env.NODE_ENV !== 'production'
})