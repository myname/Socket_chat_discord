import { createRouter, createWebHistory } from 'vue-router'

import Chat from '@/views/Chat'
import Redirect from '@/views/Redirect'
import E404 from '@/views/E404'

let routes = [
    {
        name: 'chat',
        path: '/',
        component: Chat,
        meta: {
            enterClass: 'animate__animated animate__fadeInRight',
            leaveClass: 'animate__animated animate__fadeOutLeft'
        }
    },
    {
        name: 'redirect',
        path: '/redirect',
        component: Redirect,
        meta: {
            enterClass: 'animate__animated animate__fadeInRight',
            leaveClass: 'animate__animated animate__fadeOutLeft'
        }
    },
    {
        name: 'E404',
        path: '/:pathMatch(.*)',
        component: E404,
        meta: {
            enterClass: 'animate__animated animate__fadeInRight',
            leaveClass: 'animate__animated animate__fadeOutLeft'
        }
    }
]

export default createRouter({
    routes,
    history: createWebHistory(process.env.BASE_URL)
})