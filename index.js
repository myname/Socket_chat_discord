const express = require('express')
const app = express()
const http = require('http').Server(app)
const io = require('socket.io')(http)
const fetch = require('node-fetch')
const port = 3000
const moment = require('moment')
// const axios = require('axios')
// const { v5: uuidv5 } = require('uuid')
require('dotenv').config() // хз нужно ли это тут



io.engine.on('initial_headers', (headers, req) => {
    headers['test'] = '123'
})

io.engine.on('headers', (headers, req) => {
    headers['test2'] = '789'
})

io.engine.on("connection_error", (err) => {
    console.log('\nconnection_error:')
    console.log(err.req)      // the request object
    console.log(err.code)     // the error code, for example 1
    console.log(err.message)  // the error message, for example "Session ID unknown"
    console.log(err.context)  // some additional error context
})



app.get('*', async (request, response, next) => {
    console.log(request.url)
    const { query } = request
    const { code, state } = query

    if (!code || !state) return next()

    console.log(`CODE: ${code} ! state: ${state}`)
    try {
        const { VUE_APP_CLIENT_ID, VUE_APP_CLIENT_SECRET } = process.env
        const oauthResult = await fetch('https://discord.com/api/oauth2/token', {
            method: 'POST',
            body: new URLSearchParams({
                client_id: VUE_APP_CLIENT_ID,
                client_secret: VUE_APP_CLIENT_SECRET,
                code,
                grant_type: 'authorization_code',
                redirect_uri: process.env.VUE_APP_HOST,
                scope: 'identify'
            }),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })

        const oauthData = await oauthResult.json()
        console.log(oauthData) // эти данные следует отправить на клиент для сохранения
        const socket = io.sockets.sockets.get(state)
        socket.data.oauthData = oauthData
        socket.emit('oauthData', oauthData) // кста эти данные нам вроде и не нужны будут, но пусть


        const userdatafetch = await fetch('https://discord.com/api/users/@me', {
            headers: {
                authorization: `${oauthData.token_type} ${oauthData.access_token}`,
            }
        })

        const userData = await userdatafetch.json()
        console.log(userData)
        socket.data.user = userData
        socket.emit('user', userData) // кста эти данные нам вроде и не нужны будут, но пусть

        // io.emit('connectUser', onlineCount) // обновляем данные у всех
        // io.to("some room").emit("some event")
        // io.sockets.get(id)
        // io.fetchSockets(id)
        // socket.data // сохранить тут данные
        // отправить событие с данными пользователю и сохранить данные тут в сокете
        // как послать данные на клиент с этой точки? (без io)
        // да никак, они могут только храниться по id и когда будет идти авторизация, сокета то там прицепить данные
        // - что есть открывать в новом окне, таким образом коннект сохранится (но что если чел дисконектится)
    } catch(err) {
        console.log(err)
    }

    // что если тут всегда вручную возвращать папку dist
    // res.send(`${__dirname}/dist/index.html`)
    return next()
})


app.use(express.static(`${__dirname}/dist`))
// app.set('views', __dirname + '/dist')
// app.get('/redirect', function(req, res){
//     res.render('views/redirect');
// })


// app.use((req, res, next) => {
//     console.log(req.hostname)
//     if (req.hostname === 'webmyself2.ru') { 
//         return next()
//     }
// });


io.on('connection', async (socket) => { // вызывается для каждого подключенного
    try {
        // console.log(socket)
        // socket.data.username // передавать данные между серверами
        // socket.join("some room") // комната
        // io.to("some room").emit("some event")
        // io.to("room1").to("room2").to("room3").emit("some event") // каждый получит только 1 сообщение (обьединение будет)
        // socket.to("some room").emit("some event") // каждый кто в комнате, кроме отправителя (владельца сокета)
        // socket.on('private message', (socketId, message) => socket.to(id).emit(socketId, message))
        // console.log(socket.rooms)

        // сделать проверку на повторяющиеся ip, что бы избежать много открытых окон с одного ip


        socket.use(([event, ...args], next) => { // event и args это из socket
            console.log(`event:`, event) // тут можно сделать проверку на неавторизованные методы/подключения
            if (event == 'err') return new Error('my err test')
            next()
        })

        io.use((socket, next) => {
            const token = socket.handshake.auth
            console.log(`token: ${token}`, token)
            next()
        })

        socket.on('error', err => { // а тут ловим все ошибки
            if (err?.message === 'my err test') socket.disconnect() // отключит клиента
        })


        console.log('connect')
        socket.on("ping", (count) => {
            console.log(count)
        })


        io.emit('chat message to client', {msg: 'joined', user: socket.data?.user, type: 'system', date: moment.utc()})
        const sockets = await io.fetchSockets()
        io.emit('connectUser', sockets.length)

        socket.on('chat message to server', msg => {
            try {
                if (!msg) throw new Error('Сообщение не может быть пустым (1).')
                msg = msg.trim()
                if (!msg) throw new Error('Сообщение не может быть пустым (2).')
                if (msg.length > 2000) throw new Error('Превышен лимит длины сообщения.')
                console.log('backend', msg, +moment.utc())
                io.emit('chat message to client', {msg, user: socket.data?.user, type: 'message', date: moment.utc()})
            } catch(err) {
                // передаем ошибку пользователю io.emit('error_to_client', err.message)
                io.emit('error_to_client', err.message)
            }
        })

        socket.on('disconnect', async socket => {
            const sockets = await io.fetchSockets()
            io.emit('connectUser', sockets.length)
            io.emit('chat message to client', {msg: 'leave', user: socket.data?.user, type: 'system', date: moment.utc()})
            console.log(`${socket.id} leave`)
        })
    } catch(err) {
        try {
            io.emit('error_to_client', err.message)
        } catch(e) {
            console.error(e)
        }
    }
})


io.of("/").adapter.on("create-room", (room) => {
    console.log(`room ${room} was created`)
})

io.of("/").adapter.on("join-room", (room, id) => {
    console.log(`socket ${id} has joined room ${room}`)
})



http.listen(port, () => {
    console.log(`Socket.IO server running at http://localhost:${port}/`)
})





class Myobj {
    #size = 0 // размер
    #order = new Set() // порядок добавления
    constructor() {
        // код преобразования обьекта (двумерного массива) в нужную структуру
    }

    _set(key, value) {
        // как быть, если уже есть такой эллемент? нужно удалить из order и записать заного?
        const has = this.#order.has(key)
        if (has) this.#order.delete(key) // уже есть, удаляем
        this[key] = value
        if (!has) this.#size++
        this.#order.add(key)
    }

    _delete(key) {
        delete this[key]
        this.#order.delete(key)
        this.#size--
    }

    _has(key) {
        return this.#order.has(key)
    }

    _size() {
        return this.#size
    }

    _forEach(callback) {
        this.#order.forEach(key => {
            callback(key, this[key])
        })
        return this
    }
}


/**
 * 
 * @param {Number} time - длительность мс
 * @param {Number} gap - переодичность увеличения мс
 * @return {Object} { on, start, timer }
 */
function proTimer(time, gap) {
    let gapFunc = function() {}
    let gapPrefFunc = function() {}
    let endFunc = function() {}
    let launched = false
    const th = { on, start, stop } // this

    function on(type, callback) {
        if (type === 'gap') {
            gapFunc = callback
        } else if (type === 'gapPref') {
            gapPrefFunc = callback
        } else if (type === 'end') {
            endFunc = callback
        }
        return th
    }

    let gapCount = 0
    const gapMax = Math.floor(time / gap)
    const ost = time % gap

    function runTimer() {
        const time = gapCount < gapMax ? gap : ost
        gapPrefFunc({gap, count: gapCount, time, type: 'gapPref'})
        return setTimeout(() => {
            gapCount++
            gapFunc({gap, count: gapCount, time, type: 'gap'})
            if (gapCount < gapMax) {
                th.timer = runTimer() // запускаем новый таймер (цикл)
            } else {
                endFunc({gap, count: gapCount, time, type: 'end'})
                gapCount = 0
                th.timer = null
                launched = false
            }
        }, time)
    }

    function start() {
        if (launched) throw new Error('Уже запущен.')
        launched = true
        th.timer = runTimer()
    }

    function stop() {
        clearTimeout(th.timer)
        gapCount = 0
        th.timer = null
        launched = false
    }

    return th
}